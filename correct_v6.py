import glob
import sys,os
from datetime import datetime as dt
import pickle

# to be done

# include cc_half in alternative resolution
# check length maximum sample name, adjust table
# automatic run python 3 instead of 2
# dict inici amb numero
# test it with python 2
# check the presence of modules before executing

THRESHOLD_CC = 50
THRESHOLD_CC_MIN = 40

START_BOLD = '\033[1m'
RED     = '\033[31m'
END_BOLD = '\033[0m'

no_data = '----'

def extraImport():
    
    # tries to import non built in modules. Returns True if managed, False otherwise

    tag = True

    try:
        import pandas as pd

    except:

        print('cannot import pandas')
        tag = False

    try:

        from openpyxl import Workbook

    except:

        print('cannot import openpyxl')

        tag = False
   
    return tag

def checkInteger(number):

    # checks if number is integer (makes it integer in case of comes as string)

    try:
        number = int(number)

    except:
        
        number = None

    return number

def tableGroup(number):

    # converts the group space number (e.g 1) into group space (e.g P1)

    group = 'not found'
    
    possible_numbers = [1,3,4,5,16,17,18,19,21,20,22,23,24,75,76,77,78,89,90,91,92,93,94,
                        95,96,79,80,97,98,143,144,145,149,150,151,152,153,154,168,169,170,
                        171,172,173,177,178,179,180,181,182,146,155,195,198,207,208,212,213,
                        196,209,210,197,199,211,214]
    dict_groups = {

        'aP':[[1,'P1']],
        'mP':[[3,'P2'],[4,'P2(1)']],
        'mC,mI':[[5,'C2']],
        'oP':[[16,'P222'],[17,'P222(1)'],[18,'P2(1)2(1)2'],[19,'P2(1)2(1)2(1)']],
        'oC':[[21,'C222'],[20,'C222(1)']],
        'oF':[[22,'F222']],
        'oI':[[23,'I222'],[24,'I2(1)2(1)2(1)']],'tP':[[75,'P4'],[76,'P4(1)'],[77,'P4(2)'],
             [78,'P4(3)'],[89,'P422'],[90,'P42(1)2'],[91,'P4(1)22'],[92,'P4(1)2(1)2'],
             [93,'P4(2)22'],[94,'P4(2)2(1)2'],[95,'P4(3)22'],[96,'P4(3)2(1)2']],
        'tI':[[79,'I4'], [80,'I4(1)'], [97,'I422'],[98,'I4(1)22']],
        'hP':[[143,'P3'],[144,'P3(1)'],[145,'P3(2)'],[149,'P312'],[150,'P321'],[151,'P3(1)12'],
             [152,'P3(1)21'],[153,'P3(2)12'],[154,'P3(2)21'], [168,'P6'], [169,'P6(1)'],
             [170,'P6(5)'], [171,'P6(2)'], [172,'P6(4)'], [173,'P6(3)'], [177,'P622'],
             [178,'P6(1)22'], [179,'P6(5)22'], [180,'P6(2)22'], [181,'P6(4)22'], [182,'P6(3)22']],
        'hR':[[146,'R3'], [155,'R32']],
        'cP':[[195,'P23'],[198,'P2(1)3'],[207,'P432'],[208,'P4(2)32'], [212,'P4(3)32'], [213,'P4(1)32']],
        'cF':[[196,'F23'],[209,'F432'],[210,'F4(1)32']],
        'cI':[[197,'I23'],[199,'I2(1)3'],[211,'I432'],[214,'I4(1)32']]

        }

    number = checkInteger(number)

    if number in possible_numbers:

        for key in dict_groups:

            list_groups = dict_groups[key]

            for group_l in list_groups:
 
            
               if number in group_l:
                
                    group = group_l[1]
                    break

    else:

        print('ERROR: Number of Group not found! Input parameter was %s'%number)    

    return group


def getDate():
    
    # formats the time for the output file name.

    now = dt.now()
    time_format = now.strftime("%d_%m_%y_%Hh_%Mmin")
    time_format = str(time_format)

    return(time_format)

def findPaths(path):
    
    # provides an ordered list of paths to inspect.
    # path is the main path were all corrects are.

    list_paths=[]
    
    if os.path.exists(path):

        for f in glob.glob('%s/**/CORRECT.LP'%path, recursive=True):

            list_paths.append(f)

        list_paths.sort()

    else:

        print('ERROR: Path provided does not exist, please check. Given path: %s'%path)

    return list_paths

def _results2file(data, name_file, path):

    # opens/creates a file, stores the results of the inspect.
    # data refers to the correct 'table' ?
    # path is the main path

    f = open(name_file, 'a')
   
    index_resolution = []
    index_end_resolution = []
    
    f.write('\n#####################################################################################################################################################\n')
    f.write(path)
    f.write('\n')

    for index,line in enumerate(data):

        if (line.find('***** CORRECT *****')) != -1:

           f.write(data[index])

        if (line.find('NAME_TEMPLATE_OF_DATA_FRAMES=')) != -1:

           f.write(data[index])
    
        if (line.find('THE DATA COLLECTION STATISTICS REPORTED BELOW ASSUMES:')) !=-1:

            f.write(data[index+1])
 
        if (line.find(' RESOLUTION     NUMBER OF REFLECTIONS    COMPLETENESS R-FACTOR  R-FACTOR COMPARED I/SIGMA   R-meas  CC(1/2)  Anomal  SigAno   Nano')) !=-1:
            
            index_resolution.append(index)

        if (line.find('NUMBER OF REFLECTIONS IN SELECTED SUBSET OF IMAGES')) != -1:

            index_end_resolution.append(index)
    try:

        last_index_resolution = index_resolution[-1]
        last_index_end_resolution = index_end_resolution[-1]

        last_resolution = (data[last_index_resolution:last_index_end_resolution])

        for line in last_resolution:
  
            f.write(line)

    except:

        print('\nCould not inspect file %s\n'%line)

    f.close()


def locateSpace(path, main_path):
    
    # creates a name for the output file with the current date
    # path to particular correct
    # main_path to all corrects
    # returns the data read from a particular correct path

    time_stamp = getDate()

    name_file = main_path.replace('/','_')
    name_file = name_file.replace('.','_')
    name_file = ('output_%s_%s.txt'%(name_file,time_stamp))

    if 'CORRECT.LP' in path:

        data = open('%s'%path, 'r').readlines()
   
    else:
   
        data = open('%s/CORRECT.LP'%path, 'r').readlines()

    return data, name_file, path


def printInspectionSummary(paths):

    print('\n%i CORRECTS found.\n'%(len(paths)))

    for i,path in enumerate(paths):

        print('path %i: %s'%(i+1, path))

    print('\n')


def inspectCorrect():

    name_file = '' # solve this in case no path is provided
    paths = '' # IDEM
    version_python = sys.version_info[0]

    if version_python == 2:

        print('\nPlease run python 3 instead of python 2\ntype:\npython3 correct_instpect.py PATH_TO_DATA')

    try:  

        if len(sys.argv) == 2:

            main_path = sys.argv[1]

            if os.path.exists(main_path):
                paths = findPaths(main_path)
                for path in paths:
                   data, name_file, path = locateSpace(path, main_path) 
                   _results2file(data, name_file, path)
            else:

                print('I cannot find the path that you input. Given path: %s'%main_path)
            

        else:

            print('path not valid\ne.g.\n')
            print('python correct_inspect.py .')

    except OSError as err:

        print('\nCannot run the script\n')
        print(err)

    return name_file, paths

def printContentFile(file_name):

    file_output = open(file_name).readlines()
    for line in file_output:
        print(line)

def filterResult(file_output):
 
    data_output = open(file_output).readlines()
    for line in data_output:
        print(line.split())
    
    # continue here # 

def getDataOutput(file_output):

    data_output = open(file_output).readlines()
    return data_output


def _resCCHalf(list_table):

    resolution = []
    cc_half = []

    for line in list_table:
        line_split = line.split()
        resolution.append(line_split[0])
        if '*' in line_split[10]:
            cc = line_split[10].replace('*','')
            cc_half.append(float(cc))
        else:
            cc_half.append(float(line_split[10]))

    return resolution, cc_half

def _identifyResolution(resolution, cc_half):
    # only if all are > 50
    return (all(x>THRESHOLD_CC for x in cc_half))

def _selectResolution(resolution, cc_half):
    # if less than 50 is present
    for index, element in enumerate(cc_half):
        if (element) < THRESHOLD_CC:
            return index 

def _identifyFailure(resolution, cc_half):
    # only if all are < 40
    return (all(x < THRESHOLD_CC_MIN for x in cc_half))

def getResolutions(data_output):

    selected_indexes = []
    boundary_indexes = []
    dict_summary = {}

    for i,line in enumerate(data_output):

        dict2dict = {}
        line_split = (line.split())

        if 'CORRECT.LP' in line:

          dict2dict['CORRECT.LP']= line

          path2dataset = line.strip()

        if 'SPACE_GROUP_NUMBER=' in line:

            space_group = line_split[1]

        if 'NAME_TEMPLATE_OF_DATA_FRAMES=' in line:
            try:
                sample_name = line.split('NAME_TEMPLATE_OF_DATA_FRAMES=')[1].split(' GENERIC')[0]
            except:
                sample_name = line

        if (line_split) == ['LIMIT', 'OBSERVED', 'UNIQUE', 'POSSIBLE', 'OF', 'DATA', 'observed', 'expected', 'Corr']:

            start_index = i + 2

        if len(line_split) > 2:

            if line_split[0] == 'total':
                end_index = i
                resolution, cc_half = _resCCHalf(data_output[start_index:end_index])

                dict2dict['SPACE_GROUP_NUMBER='] = space_group
                dict2dict['space_group'] = tableGroup(int(space_group))
                
                # case that finishes with max resolution:

                if _identifyResolution(resolution, cc_half): # if ALL cc_half are greater than 50

                    selected_indexes.append(i-1)
                    dict2dict['resolution'] = resolution[-1] # picks the last one
                    dict2dict['alternative_resolution'] = no_data
                    dict2dict['cc_half'] = cc_half[-1]
                    dict2dict['cc_half_alternative'] = no_data
                    dict2dict['path'] = path2dataset.strip()
                    dict2dict['sample_name_long'] = sample_name.strip()
                    dict2dict['sample_name_short'] = sample_name.strip().split('/')[-1]
                                  
                    dict_summary[path2dataset] = dict2dict

                else:  # not all cc_half are greater than 50.

                    index_middle = _selectResolution(resolution, cc_half)
                    selected_indexes.append(start_index + index_middle - 1)

                    if _identifyFailure(resolution, cc_half): 
                        # if ALL cc_half are less than 40

                        dict2dict['resolution'] = 'failed'
                        dict2dict['alternative_resolution'] = no_data
                        dict2dict['cc_half'] = cc_half[index_middle-1]
                        dict2dict['cc_half_alternative'] = no_data

                    else:

                        if index_middle == 0: # the first one
                                
                            if (THRESHOLD_CC_MIN < float(cc_half[0]) < THRESHOLD_CC):
                                 dict2dict['alternative_resolution'] = resolution[0]
                                 dict2dict['resolution'] = no_data
                                 dict2dict['cc_half'] = cc_half[0]
                                 dict2dict['cc_half_alternative'] = no_data
                                 
            
                        else: #not the first one

                            dict2dict['resolution'] = resolution[index_middle-1]
                            dict2dict['cc_half'] = cc_half[index_middle-1]
                            
                            
                            if THRESHOLD_CC_MIN < float(cc_half[index_middle]) < THRESHOLD_CC: 

                                dict2dict['alternative_resolution'] = resolution[index_middle]
                                dict2dict['cc_half_alternative'] = cc_half[index_middle] 

                            else:

                                dict2dict['alternative_resolution'] = no_data 
                                dict2dict['cc_half_alternative'] = no_data            

                    dict2dict['path'] = path2dataset.strip()
                    dict2dict['sample_name_long'] = sample_name.strip()
                    dict2dict['sample_name_short'] = sample_name.strip().split('/')[-1]

                    dict_summary[path2dataset] = dict2dict

                    try:

                        if THRESHOLD_CC_MIN < cc_half[index_middle] < THRESHOLD_CC:

                            boundary_indexes.append(start_index + index_middle)

                    except:

                            pass
   
    return selected_indexes, boundary_indexes, dict_summary

def printBold(data_output, selected_indexes, boundary_indexes, dict_summary):


    for j,line in enumerate(data_output):

        if j in selected_indexes:

            print(START_BOLD + data_output[j] + END_BOLD)

        else:

            if j in boundary_indexes:

                print(START_BOLD + RED + data_output[j] + END_BOLD)

            else:

                print(line)


def printInfo(dict_summary):

    #selected_indexes, bi, dict_summary = getResolutions()

    for num, key in enumerate(dict_summary.keys()):

         print('%i  --> %s' %(num, key.strip()))



def exportExcel(dict_summary):

    workbook = Workbook()
    sheet = workbook.active
    #selected_indexes, bi, dict_summary = getResolutions()

    sheet['A1'] = 'PATH'
    sheet['B1'] = 'SAMPLE NAME' 
    sheet['C1'] = 'RESOLUTION'
    sheet['D1'] = 'CC_HALF'
    sheet['E1'] = 'SPACE GROUP NUMBER'
    sheet['F1'] = 'SPACE GROUP'   

    for i,key in enumerate(dict_summary):
 
        sheet["A%i"%(i+2)] = key.strip()
        sheet['B%i'%(i+2)] = dict_summary[key]['sample_name_short']
        sheet['C%i'%(i+2)] = dict_summary[key]['resolution']
        sheet['D%i'%(i+2)] = dict_summary[key]['cc_half']
        sheet['E%i'%(i+2)] = dict_summary[key]['SPACE_GROUP_NUMBER=']
        sheet['F%i'%(i+2)] = dict_summary[key]['space_group']

    workbook.save(filename='excel_text.xlsx')

def tableSummary(dict_summary):

    # not being used due to pandas

    table_summary = dict_summary
    tablePD = pd.DataFrame(table_summary)
    print(tablePD)

def _shortenPath(path):

    if '/processed/' in path:

        path = path.split('/processed/')[1]
        path = '/processed/%s'%path

    else:
       pass
    return path

def _spacesTitle():

        #                sample_name                    resolution     cc_half  space_group  space_group_number  alternative_resolution

        sp_1 = 15
        sp_2 = 20
        sp_3 = 5
        sp_4 = 2
        sp_5 = 2
        sp_6 = 5

        return sp_1, sp_2, sp_3, sp_4, sp_5, sp_6

def _spacesData(index, sample_name_short, space_group, space_group_number, cc_half, resolution):

       # 1       sample_12_001_??????.h5                   1.50            97.8            P2(1)2(1)2(1)    19          ----   ./all_corrects/another_path/CORRECT.LP

       sp_index_sample = 3   # index - sample
       space_sample = 40


       if len(str(index)) == 1:

           sp_11 = sp_index_sample

       if len(str(index)) == 2:

           sp_11 = sp_index_sample - 1

       if len(str(index)) == 3:

            sp_11 = sp_index_sample - 2

       samplel = len(sample_name_short)

       sp_22 = space_sample - samplel

       if resolution == 'failed':

           sp_33 = 10-2

       else:

           sp_33 = 10

       sp_44 = 10
       space_groupl = len(space_group)
       sp_55 = 15 - space_groupl
       sgnl = len(space_group_number)
       sp_66 = 10 - sgnl
       cchl = len(str(cc_half))
       sp_77 = 5 - cchl

       return sp_11, sp_22, sp_33, sp_44, sp_55, sp_66, sp_77

def printDictionary(dict_summary):

    '''
    Prints a summary table with the results.
    '''

    for order,key in enumerate(dict_summary.keys()):

       for item in dict_summary[key]:
          
          sample_name_short = dict_summary[key]['sample_name_short']
          space_group = dict_summary[key]['space_group']
          space_group_number = dict_summary[key]['SPACE_GROUP_NUMBER=']
          resolution = dict_summary[key]['resolution']
          cc_half = dict_summary[key]['cc_half']
          cc_half_alternative = dict_summary[key]['cc_half_alternative']
          alternative_resolution = dict_summary[key]['alternative_resolution']

       
          
       if order == 0:
   
           horizontal = 150

           sp_1, sp_2, sp_3, sp_4, sp_5, sp_6 = _spacesTitle()

           print(" "*sp_1 + 'sample_name' + " "*sp_2 +  'resolution' + " "*sp_3 + 'cc_half'+ " "*sp_4  + 'space_group' + " "*sp_5 +'space_group_number'+ " "*sp_5 + 'alternative_resolution' )
           print('-'*horizontal)

       index = order + 1

       sp_11, sp_22, sp_33, sp_44, sp_55, sp_66, sp_77 = _spacesData(index,  sample_name_short, space_group, space_group_number, cc_half, resolution)

       ####

       if cc_half_alternative != no_data:

           alternative_resolution = '%s (%s)'%(alternative_resolution,cc_half_alternative)       


       ####
       if resolution == 'failed':

           print(RED)
           print(index," "*sp_11,sample_name_short," "*sp_22,resolution,
                " "*sp_33,cc_half, " "*sp_44, space_group,
                " "*sp_55,space_group_number," "*sp_66,alternative_resolution, " "*sp_77,
                _shortenPath(key))
           print(END_BOLD)


       else:

           if index % 2 == 0:

               print(START_BOLD)
               print(index," "*sp_11,sample_name_short," "*sp_22,resolution,
                    " "*sp_33,cc_half, " "*sp_44, space_group,
                    " "*sp_55,space_group_number," "*sp_66,alternative_resolution, " "*sp_77, 
                    _shortenPath(key))
               print(END_BOLD)

           else:

               print(index," "*sp_11,sample_name_short," "*sp_22,resolution,
                     " "*sp_33,cc_half, " "*sp_44, space_group,
                     " "*sp_55,space_group_number," "*sp_66,alternative_resolution, " "*sp_77, 
                     _shortenPath(key))


def savePKL(dict_input):

    time_now = getDate()
    if len(dict_input.keys()) == 1:

        listName = list(dict_input.keys())
        target = listName[0]
        target = reName(target)
        print(target)

    else:
        target = 'None'

    file_name = '%s_%s.pkl'%(target, time_now)

    with open(file_name, 'wb') as f:
        pickle.dump(dict_input, f)

    path_file = '%s/%s' %(os.getcwd(), file_name)

    if os.path.isfile(path_file):
        return ('File created in path %s'%path_file)
    else:
        print('File could not be created')



def main():

    name_file, paths = inspectCorrect()
    file_output = name_file
    data_output = getDataOutput(file_output)
    selected_indexes, boundary_indexes, dict_summary = getResolutions(data_output)
    
    printBold(data_output, selected_indexes, boundary_indexes, dict_summary)
    printInspectionSummary(paths)
    
    printDictionary(dict_summary)
    savePKL(dict_summary)  

    #extraImport() # if fails, cancel the two comming;
    #printInfo(dict_summary)
 
    #print(extraImport())

    #if extraImport():

        #tableSummary(dict_summary)
        #exportExcel(dict_summary)

if __name__ == '__main__':

    try:

        main()

    except OSError as err:

        if len(sys.argv)>=1:
            #main_path = sys.argv[1]
            #print('I cannot find the path that you input. Given path: %s'%main_path)        
            pass

        else:

           print('/nPut the path were CORRECT files are. e.g python correct.py PATH/n')        
          
        print(err)
    print('#########################     DONE     ##################################')
