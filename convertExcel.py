


import pickle
from openpyxl import Workbook
import sys



def getDict(dict_a_file):

    dict_1 = open(dict_a_file, 'rb')

    dict_a = pickle.load(dict_1)

    return dict_a


def exportExcel(dict_summary, name_file):

    workbook = Workbook()
    sheet = workbook.active

    sheet['A1'] = 'PATH'
    sheet['B1'] = 'SAMPLE NAME'
    sheet['C1'] = 'RESOLUTION'
    sheet['D1'] = 'ALTERNATIVE RESOLUTION'
    sheet['E1'] = 'CC_HALF'
    sheet['F1'] = 'SPACE GROUP NUMBER'
    sheet['G1'] = 'SPACE GROUP'
 
    for i,key in enumerate(dict_summary):

        sheet["A%i"%(i+2)] = key.strip()
        sheet['B%i'%(i+2)] = dict_summary[key]['sample_name_short']
        sheet['C%i'%(i+2)] = dict_summary[key]['resolution']
        sheet['D%i'%(i+2)] = dict_summary[key]['alternative_resolution']
        sheet['E%i'%(i+2)] = dict_summary[key]['cc_half']
        sheet['F%i'%(i+2)] = dict_summary[key]['SPACE_GROUP_NUMBER=']
        sheet['G%i'%(i+2)] = dict_summary[key]['space_group']

    name_file = name_file.split('.pkl')[0]
    workbook.save(filename='%s.xlsx'%name_file)

def main(dict_a_file):

    dict_summary = getDict(dict_a_file)
    exportExcel(dict_summary, dict_a_file)

if __name__ == '__main__':

        if len(sys.argv) > 1:
            dict_a_file = sys.argv[1]    # 'None_03_09_21_09h_51min.pkl' 
            main(dict_a_file)


