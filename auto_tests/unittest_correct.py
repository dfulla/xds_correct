import unittest
import sys

sys.path.append('..')

#correct_file = 'correct_v6'

from correct_v6 import (tableGroup,
                        getDate,
                        findPaths)


class TestTableGroup(unittest.TestCase):

    def setUp(self):   
   
        # used as __init__ for unittest

        self.possible_numbers = [1,3,4,5,16,17,18,19,21,20,22,23,24,75,76,77,78,89,90,91,92,93,94,
                            95,96,79,80,97,98,143,144,145,149,150,151,152,153,154,168,169,170,
                            171,172,173,177,178,179,180,181,182,146,155,195,198,207,208,212,213,
                            196,209,210,197,199,211,214]

        self.dict_groups_correct = {1: 'P1', 3: 'P2', 4: 'P2(1)', 5: 'C2', 16: 'P222', 17: 'P222(1)',
                              18: 'P2(1)2(1)2', 19: 'P2(1)2(1)2(1)', 21: 'C222', 20: 'C222(1)',
                              22: 'F222', 23: 'I222', 24: 'I2(1)2(1)2(1)', 75: 'P4', 76: 'P4(1)',
                              77: 'P4(2)', 78: 'P4(3)', 89: 'P422', 90: 'P42(1)2', 91: 'P4(1)22',
                              92: 'P4(1)2(1)2', 93: 'P4(2)22', 94: 'P4(2)2(1)2', 95: 'P4(3)22',
                              96:'P4(3)2(1)2', 79: 'I4', 80: 'I4(1)', 97: 'I422', 98: 'I4(1)22', 143: 'P3',
                              144: 'P3(1)', 145: 'P3(2)', 149: 'P312', 150: 'P321', 151: 'P3(1)12',
                              152: 'P3(1)21', 153: 'P3(2)12', 154: 'P3(2)21', 168: 'P6', 169: 'P6(1)',
                              170: 'P6(5)', 171: 'P6(2)', 172: 'P6(4)', 173: 'P6(3)', 177: 'P622',
                              178: 'P6(1)22', 179: 'P6(5)22', 180: 'P6(2)22', 181: 'P6(4)22', 182: 'P6(3)22',
                              146: 'R3', 155: 'R32', 195: 'P23', 198: 'P2(1)3', 207: 'P432', 208: 'P4(2)32',
                              212: 'P4(3)32', 213: 'P4(1)32', 196: 'F23', 209: 'F432', 210: 'F4(1)32',
                              197: 'I23', 199: 'I2(1)3', 211: 'I432', 214: 'I4(1)32'}

    def test_tableGroup(self):
      
        # checks all groups are correct
    
        dict_groups = {}
        for number in self.possible_numbers:

            dict_groups[number] = tableGroup(number)

        self.assertEqual(self.dict_groups_correct, dict_groups)

    def test_WrongNumber(self):

        # checks when numbers are not found        
    
        self.assertEqual(tableGroup(50), 'not found')

    def test_StringNumber(self):

        self.assertEqual(tableGroup('1'),'P1')

    def test_NoNumber(self):
 
        self.assertEqual(tableGroup('a'), 'not found')
        
    def test_FloatNumber(self):

        self.assertEqual(tableGroup('1.1'),'not found')

    def test_NegativeNumber(self):

        self.assertEqual(tableGroup(-1),'not found')

    def test_NoneNumber(self):

        self.assertEqual(tableGroup(None),'not found')

class TestGetDate(unittest.TestCase):
 
   def setUp(self):

       self.date_now = getDate()

   def test_dateString(self):
       
       self.assertIs(type(self.date_now), str)

   def test_dateContains_(self):
       
       self.assertTrue(len(self.date_now.split('_')) == 5)

class TestfindPaths(unittest.TestCase):

   def setUp(self):
       # there should not be any correct in this directory or tree bellow
       self.noPath = findPaths('.')
       # there should be correct files in previous directory
       self.paths = findPaths('../')

   def test_findPathsEmptyList(self):
       # check if delivers a list
       self.assertIs(type(self.noPath), list)

   def test_findPathsList(self):

       self.assertIs(type(self.paths), list)

   def test_emptyList(self):
    
       self.assertTrue(len(self.noPath)==0)

   def test_fullList(self):

       self.assertTrue(len(self.paths)!=0)

   def test_wrongPath(self):
       wrong_path = findPaths('does_not_exist')
       self.assertEqual(wrong_path, [])

def run_tests():
    # not used
    test_classes_to_run = [TestTableGroup,
                           TestTable2
                           ]

if __name__ == '__main__':

    unittest.main()
